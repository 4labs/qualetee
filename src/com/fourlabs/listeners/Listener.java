package com.fourlabs.listeners;

import java.io.IOException;
import java.text.ParseException;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.fourlabs.SuiteData;
import com.fourlabs.report.ExtentReport;
import com.fourlabs.report.HTMLReportGenerator;
import com.fourlabs.utils.TimeUtils;

public class Listener implements ITestListener {
	
	

	@Override
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		ExtentReport.tearDown();
		System.out.println("execution completed");
		SuiteData.executionEndTime=TimeUtils.getDateTime();
		try {
			SuiteData.executionTime=TimeUtils.getExecutionTime(SuiteData.executionStartTime, SuiteData.executionEndTime);
			new HTMLReportGenerator().createExecutionReport();
		} catch (ParseException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onStart(ITestContext arg0) {
		SuiteData.passCount=0;
		SuiteData.failCount=0;
		SuiteData.executionStartTime=TimeUtils.getDateTime();
		
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult arg0) {
		// TODO Auto-generated method stub
		System.out.println("test fail");
		
		
	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
		System.out.println("test start");
		
		
		
	}

	@Override
	public void onTestSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub
	
		System.out.println("test success");
		
		
	}

	

	

}
