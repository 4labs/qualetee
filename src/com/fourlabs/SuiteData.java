package com.fourlabs;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.simple.JSONObject;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.fourlabs.dto.ModuleLevel;
import com.fourlabs.dto.ObjectRepo;
import com.fourlabs.dto.StepLevel;

public class SuiteData {

	public static WebDriver driver;
	public static HashMap<String, ObjectRepo> objMap;
	public static HashMap<String, ModuleLevel> reportMap;
	public static HashMap<String,LinkedHashMap<String, StepLevel>> moduleStepMap;
	public static LinkedHashMap<String, StepLevel> stepMap;
	public static ModuleLevel report;
	public static StepLevel step;
	public static JSONObject json;
	public static int passCount;
	public static int failCount;
	public static String executionStartTime;
	public static String executionEndTime;
	public static String executionTime;
	public static int stepID;
	public static ExtentReports extent;
	

}
