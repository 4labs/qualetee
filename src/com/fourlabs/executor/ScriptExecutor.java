package com.fourlabs.executor;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.fourlabs.SuiteData;
import com.fourlabs.dto.ModuleLevel;
import com.fourlabs.dto.StepLevel;
import com.fourlabs.report.ExtentReport;
import com.fourlabs.utils.ExcelUtils;
import com.fourlabs.utils.FilePath;
import com.fourlabs.utils.ObjectUtils;
import com.fourlabs.utils.TimeUtils;

import Exception.FilloException;
import Fillo.Recordset;

public class ScriptExecutor {

	/**
	 * @param args
	 * @throws Throwable 
	 */
	public void readAndExecuteScript(String testCaseName) throws Throwable 
	{
		ExtentReport.createTest(testCaseName);
		SuiteData.report=new ModuleLevel();
		String stepID = null;
		SuiteData.report.setTestCaseName(testCaseName);
		SuiteData.report.setStartTime(TimeUtils.getDateTime());
		SuiteData.report.setStatus("pass");
		SuiteData.moduleStepMap= new HashMap<String,LinkedHashMap<String,StepLevel>>();
		//new ObjectUtils().setObjMap();
		ExcelUtils utils=new ExcelUtils();
		Recordset record=utils.filterWorkSheet("Steps", "TestCaseID", testCaseName);
		SuiteData.stepMap= new LinkedHashMap<String,StepLevel>();
		try
		{
			SuiteData.stepID=0;
			new ScriptExecutor().executeScript(record);
			SuiteData.moduleStepMap.put(testCaseName, SuiteData.stepMap);
			SuiteData.report.setStepMap(SuiteData.moduleStepMap);
			SuiteData.passCount++;
		}
		catch(Exception e)
		{
			
			SuiteData.failCount++;
			System.out.println("script execution failed");
			
			SuiteData.report.setStatus("fail");
			SuiteData.moduleStepMap.put(testCaseName, SuiteData.stepMap);
			SuiteData.report.setStepMap(SuiteData.moduleStepMap);
			throw new Exception();
			
		}
		finally
		{
			
			SuiteData.report.setEndTime(TimeUtils.getDateTime());
			SuiteData.report.setExecutionTime(TimeUtils.getExecutionTime(SuiteData.report.getStartTime(), SuiteData.report.getEndTime()));
			SuiteData.reportMap.put(testCaseName, SuiteData.report);
		}

	}
	public void executeScript(Recordset record) throws Exception
	{
	
		while (record.next())
		 {
			
			String stepID= record.getField("StepNo");
			SuiteData.stepID++;
			SuiteData.step=new StepLevel();
			SuiteData.step.setAction(record.getField("Keyword"));
			SuiteData.step.setStepId(record.getField("StepNo"));
			try{
			Class cls=Class.forName("com.fourlabs.executor.KeywordExecutor");
			Method method=cls.getDeclaredMethod(record.getField("Keyword"), Recordset.class);
			method.invoke(new KeywordExecutor(), record);
			SuiteData.step.setStatus("Pass");
			SuiteData.stepMap.put(SuiteData.stepID+"", SuiteData.step);
			}
			catch(Exception e)
			{
				
				SuiteData.step.setStatus("Fail");
				SuiteData.stepMap.put(stepID, SuiteData.step);
				throw new Exception();
			}
			
			
		}

	}

}
