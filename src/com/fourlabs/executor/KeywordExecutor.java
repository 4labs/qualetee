package com.fourlabs.executor;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.JsonSyntaxException;
import com.fourlabs.SuiteData;
import com.fourlabs.dto.StepLevel;
import com.fourlabs.utils.ExcelUtils;
import com.fourlabs.utils.JsonReader;
import com.fourlabs.utils.ObjectUtils;
import com.fourlabs.utils.ValueConverter;

import Exception.FilloException;
import Fillo.Recordset;

public class KeywordExecutor {
	
	ObjectUtils obj=new ObjectUtils();
	ValueConverter value=new ValueConverter();

	public void navigate(Recordset rs) throws Exception  
	{
		try
		{
		SuiteData.step.setExpectedResult("navigate to "+rs.getField("Value")+" URL");
		SuiteData.driver.get(rs.getField("Value"));
		SuiteData.step.setActualResult("Navigated to URL");
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Requested URL doesnot exist ");
			throw new Exception();
		}
	}
	public void verifyTab(Recordset rs) throws Exception
	{
		try
		{
		boolean isFound=false;
		String[] menu=rs.getField("Value").split(",");
		SuiteData.step.setExpectedResult("Verify the existence of "+menu[1]+" under "+menu[0]+" tab");
		String menuXpath="//a[text()='"+menu[0]+"']";
		String subMenu=menuXpath+"/following-sibling::ul/li";
		WebElement element=SuiteData.driver.findElement(By.xpath(menuXpath));
		Actions action=new Actions(SuiteData.driver);
		action.moveToElement(element).build().perform();
		Thread.sleep(2000);
		List<WebElement> list=SuiteData.driver.findElements(By.xpath(subMenu));
		for(WebElement elem:list)
		{
			if(elem.getText().equalsIgnoreCase(menu[1]))
			{
				isFound=true;
				break;
			}
		}
		if(isFound)
		{
			SuiteData.step.setActualResult("Verified the submenu");
		}
		else
		{
			SuiteData.step.setActualResult("Cannot find submenu "+menu[1]);
			throw new Exception();
		}
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Cannot find submenu");
			throw new Exception();
		}
		
	}
	
	public void type(Recordset rs) throws Exception
	{
		try
		{
		String data=value.formatValue(rs.getField("Value"));
		SuiteData.step.setExpectedResult("Enter the value "+data+" in the textbox");
		System.out.println(rs.getField("ObjectID"));
		WebElement textBox=obj.getElement(rs.getField("ObjectID"));
		textBox.sendKeys(data);
		SuiteData.step.setActualResult("Entered the value");
		}
		catch(Exception e)
		{
			
			SuiteData.step.setActualResult("Unable to find the textbox");
			throw new Exception();
		}
		
	}
	public void clear(Recordset rs) throws Exception
	{
		try
		{
		String data=value.formatValue(rs.getField("Value"));
		SuiteData.step.setExpectedResult("Clear the value in the textbox");
		System.out.println(rs.getField("ObjectID"));
		WebElement textBox=obj.getElement(rs.getField("ObjectID"));
		textBox.clear();
		SuiteData.step.setActualResult("Cleared the value");
		}
		catch(Exception e)
		{
			
			SuiteData.step.setActualResult("Unable to find the textbox");
			throw new Exception();
		}
		
	}
	public void executeBulkTransaction(Recordset rs) throws Throwable
	{
		JsonReader reader=new JsonReader();
		String scriptName=rs.getField("ObjectID");
		JSONArray array=reader.readTestData(rs.getField("Value"));
		for(Object obj:array)
		{
			System.out.println("inside for");
			SuiteData.json=(JSONObject)obj; 
			ExcelUtils utils=new ExcelUtils();
			ScriptExecutor script=new ScriptExecutor();
			Recordset record=utils.filterWorkSheet("Steps", "TestCaseID", scriptName);
			script.executeScript(record);
		}
			/*while (record.next())
			 {
				String stepID= record.getField("StepNo");
				SuiteData.step.setAction(record.getField("Keyword"));
				SuiteData.step.setStepId(record.getField("StepNo"));
				Class cls=Class.forName("com.stellium.executor.KeywordExecutor");
				Method method=cls.getDeclaredMethod(record.getField("Keyword"), Recordset.class);
				method.invoke(new KeywordExecutor(), record);
				SuiteData.step.setStatus("Pass");
				SuiteData.stepMap.put(stepID, SuiteData.step);
			 }*/
		
	}
	public void click(Recordset rs) throws Exception
	{
		try
		{
		SuiteData.step.setExpectedResult("Click on the element ");
		WebElement button=obj.getElement(rs.getField("ObjectID"),rs.getField("Value"));
		button.click();
		
		SuiteData.step.setActualResult("Successfully clicked on the element");
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Unable to click on the element");
			throw new Exception();
		}
	
	}
	public void mouseOver(Recordset rs) throws Exception
	{
		try
		{
		SuiteData.step.setExpectedResult("MouseOver on the element ");
		WebElement element=obj.getElement(rs.getField("ObjectID"),rs.getField("Value"));
		Actions actions=new Actions(SuiteData.driver);
		actions.moveToElement(element).build().perform();
		SuiteData.step.setActualResult("Successfully moused over on the element");
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Unable to mouse over on the element");
			throw new Exception();
		}
	
	}
	
	
	
	public void verifyElementExist(Recordset rs) throws Exception
	{
		try
		{
		SuiteData.step.setExpectedResult("Verify  the element ");
		WebElement element=obj.getElement(rs.getField("ObjectID"));
		if(element.isDisplayed())
		{
			SuiteData.step.setActualResult("Verified the element");
		}
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Element does not exist");
			throw new Exception();
		}
		
	}
	public void verifyElementNotExist(Recordset rs) throws Exception
	{
		try
		{
		SuiteData.step.setExpectedResult("Verify element "+rs.getField("ObjectID")+" does not exist ");
		WebElement element=obj.getElement(rs.getField("ObjectID"));
		if(element==null || !element.isDisplayed())
		{
			SuiteData.step.setActualResult("Element does not exist");
			
		}
		else
		{
			throw new Exception();
		}
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Element exist");
			throw new Exception();
			
		}
		
	}
	
	public void selectMenuItem(Recordset rs) throws Exception
	{
		try
		{
			String[] menu=rs.getField("Value").split(",");
			SuiteData.step.setExpectedResult("Select the submenu "+menu[1]+" from "+menu[0]+" tab");
			String menuXpath="//a[contains(text(),'"+menu[0]+"')]";
			String subMenu="//ul[@class='mega-sub-menu']/li/a[text()='"+menu[1]+"']";
			WebElement element=SuiteData.driver.findElement(By.xpath(menuXpath));
			Actions action=new Actions(SuiteData.driver);
			action.moveToElement(element).build().perform();
			Thread.sleep(2000);
			SuiteData.driver.findElement(By.xpath(subMenu)).click();
			Thread.sleep(4000);
			SuiteData.step.setActualResult("Successfully clicked on menu item");
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Unable to select the menu item");
			throw new Exception();
		}
	}
	public void verifyElementText(Recordset rs) throws Exception
	{
		try
		{
		String value=rs.getField("Value");
		SuiteData.step.setExpectedResult("Verify  the element "+SuiteData.objMap.get(rs.getField("ObjectID")).getFieldName()+ "text "+value);
		WebElement element=obj.getElement(rs.getField("ObjectID"));
		if(element.getText().equalsIgnoreCase(value))
		{
			SuiteData.step.setActualResult("Verified the element text");
		}
		}
		catch(Exception e)
		{
			SuiteData.step.setActualResult("Element text not match");
			throw new Exception();
		}
		
	}
	


}
