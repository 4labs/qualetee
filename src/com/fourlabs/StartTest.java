package com.fourlabs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import Fillo.Recordset;

import com.fourlabs.utils.ExcelUtils;
import com.fourlabs.utils.TimeUtils;

public class StartTest {

	/**
	 * @param args
	 * @throws Throwable 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws Throwable  {
		
		
		
		
		ExcelUtils utils= new ExcelUtils();
		
		 Map<String,String> testngParams = new HashMap<String,String> ();
		// TODO Auto-generated method stub
	    //Create an instance on TestNG
	     TestNG myTestNG = new TestNG();

	    //Create an instance of XML Suite and assign a name for it.
	     XmlSuite mySuite = new XmlSuite();
	     mySuite.setName("MySuite");

	    //Create an instance of XmlTest and assign a name for it.
	     XmlTest myTest = new XmlTest(mySuite);
	     myTest.setName("MyTest");

	    //Add any parameters that you want to set to the Test.
	    // myTest.setParameters(testngParams);

	    //Create a list which can contain the classes that you want to run.
	     List<XmlClass> myClasses = new ArrayList<XmlClass> ();
	     XmlClass cls=new XmlClass("com.fourlabs.test.TestClass");
	     List<XmlInclude> methodNames = new ArrayList<XmlInclude> ();
	     Recordset record=null;
	     String testCaseID=System.getenv("TestCaseID");
	     if(testCaseID!=null)
	     {
	    	 System.out.println("From jenkins");
	    	 record=utils.filterWorkSheet("Main", "TestCaseID", testCaseID);
	    	 
	     }
	     else
	     {
		     record=utils.filterWorkSheet("Main", "Run", "Yes");
	     }
	     
	     while (record.next())
	     {
	    	 
	     methodNames.add(new XmlInclude(record.getField("ScriptName")));
	     testngParams.put(record.getField("ScriptName"), record.getField("TestCaseID"));
	     }
	     
	     record.close();
	     myTest.setParameters(testngParams);
	     cls.setIncludedMethods(methodNames);
	     myClasses.add(cls);
	     myTest.setXmlClasses(myClasses);
	     //Create a list of XmlTests and add the Xmltest you created earlier to it.
	     List<XmlTest> myTests = new ArrayList<XmlTest>();
	     myTests.add(myTest);

	    //add the list of tests to your Suite.
	     mySuite.setTests(myTests);

	    //Add the suite to the list of suites.
	     List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
	     
	     mySuites.add(mySuite);

	    //Set the list of Suites to the testNG object you created earlier.
	     myTestNG.setXmlSuites(mySuites);

	    /*TestListenerAdapter tla = new TestListenerAdapter();
	    myTestNG.addListener(tla);*/
	    
	    /*TestListenerAdapter tla = new TestListenerAdapter();
	    List<Class> listenerClasses = new ArlrayList<Class>();
	    listenerClasses.add(com.petrolink.listeners.Listener.class);
	    myTestNG.setListenerClasses(listenerClasses);*/
	  //  myTestNG.addListener(com.petrolink.listeners.Listener.class);
	   // myTestNG.setUseDefaultListeners(true);
	    //myTestNG.addListener(tla);

	    //invoke run() - this will run your class.
	     myTestNG.run();
	}

}
