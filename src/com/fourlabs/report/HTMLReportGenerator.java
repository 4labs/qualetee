package com.fourlabs.report;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import com.fourlabs.SuiteData;
import com.fourlabs.dto.ModuleLevel;
import com.fourlabs.dto.StepLevel;
import com.fourlabs.utils.FilePath;

public class HTMLReportGenerator implements FilePath {
	
	

	
	public void createExecutionReport() throws IOException
	
	{
		BufferedWriter out;
		String OUT_FOLDER;
		HashMap<String, ModuleLevel> hashmap=SuiteData.reportMap;
		int passCount=SuiteData.passCount;
		int failCount=SuiteData.failCount;
		int totalCount=passCount+failCount;
		System.out.println("inside createChart");
		OUT_FOLDER = executionReport;
		PrintWriter fstream = new PrintWriter(new BufferedWriter(
		new FileWriter(new File(OUT_FOLDER, "ExecutionReport.html"))));
		out = new BufferedWriter(fstream);
		out.write("<html>");
		out.write("<head>");
		
		out.write("</head>");
		out.write("<body>");
		out.write("<table><tr><td width=\"10%\"></td><td width=\"60%\"><h1 align=\"center\">Qualetee Automation Execution Report</h1></td><td>");
	
		
		out.write(" <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>");
		out.write("<script type=\"text/javascript\">");
		out.write("google.charts.load('current', {'packages':['corechart']});");
		out.write("google.charts.setOnLoadCallback(drawChart);");
		out.write("function drawChart() {");
		out.write(" var data = google.visualization.arrayToDataTable([");
		out.write("	['Test Cases', 'Percentage'],");
		out.write("['Passed',     "+passCount+"],");
		out.write("['Failed',      "+failCount+"],");
		out.write("]);");
		out.write("  var options = {");
		out.write("	title: 'Test Execution Report',colors: ['#44aa44','#ff4444'],is3D:true");
		out.write("  };");
		out.write("var StepOptions = { legend: { position: 'top', maxLines: 1 },bar: { groupWidth: '20%' },isStacked: 'percent'};");;
		out.write(" var chart = new google.visualization.PieChart(document.getElementById('piechart'));");
		out.write(" chart.draw(data, options);");
		out.write("}");
		out.write("</script>");
		out.write(" <div id=\"piechart\" style=\"width: 400px; height: 200px; align:right;\"></div>");
		out.write("</td></tr>");
		out.write("</table>");
		out.write("<table>");
		out.write("<tr><td width=\"20%\"><b>Total TestCases: </b></td><td width=\"20%\">"+totalCount+"</td>" );
		out.write("<td width=\"20%\"><b>Start Time: </b></td><td width=\"32%\">"+SuiteData.executionStartTime+"</td>");
		out.write("<td> <a href=\"extentReport\\ExtendReport.html\">Extent Report</a></td></tr>");
		out.write("<tr><td><b>Pass Count: </b></td><td>"+passCount+"</td>");
		out.write("<td><b>End Time: </b></td><td>"+SuiteData.executionEndTime+"</td></tr>");
		
		
		out.write("<tr><td><b>Fail Count: </b></td><td>"+failCount+"</td>");	
		out.write("<td><b>Execution Time: </b></td><td>"+SuiteData.executionTime+"</td></tr></table>");	
		
		
		
				
		
		out.write("<br><br><table border = \"1\" style=\"width:100%\">");
		out.write("<tr>");
		out.write("<th>TC Name</th>");
		out.write("<th>Execution Time</th>"); 
		out.write("<th>Status</th>");
		out.write("</tr>");
		for(String key:hashmap.keySet())
		{
			createTestCaseReport(key);
			out.write("<tr>");
			out.write("<td>"+key+"</td>");
			out.write("<td>"+hashmap.get(key).getExecutionTime()+"</td>");
			if(hashmap.get(key).getStatus().equalsIgnoreCase("pass"))
			{
				out.write("<td bgcolor=\"#44aa44\"><a href=\"testCaseReport\\"+key+".html\">Passed</a></td>");
			}
			else
			{
				out.write("<td bgcolor=\"#FF0000\"><a href=\"testCaseReport\\"+key+".html\">Failed</a></td>");
			}
			out.write("</tr>");
		}
        out.write("</table>");
			
		out.write("<style>.google-visualization-table-td {text-align: center !important;}</style>");
		out.write("<div id=\"table_div\"></div>");
		out.write("</body>");
		out.write("</html>");
		out.close();
	}
	
	public void createTestCaseReport(String testCaseName) throws IOException
	{
		
		BufferedWriter out;
		String OUT_FOLDER;
		HashMap<String, StepLevel> hashmap=SuiteData.reportMap.get(testCaseName).getStepMap().get(testCaseName);
		int passCount=SuiteData.passCount;
		int failCount=SuiteData.failCount;
		System.out.println("inside createChart");
		OUT_FOLDER = testCaseReport;
		PrintWriter fstream = new PrintWriter(new BufferedWriter(
		new FileWriter(new File(OUT_FOLDER, testCaseName+".html"))));
		out = new BufferedWriter(fstream);
		out.write("<html>");
		out.write("<head>");
		
		out.write("</head>");
		out.write("<body>");
		out.write("<table><tr><td width=\"10%\"></td><td width=\"60%\"><h1 align=\"center\">"+testCaseName+"</h1></td><td>");
		out.write("</td></tr>");
		
		out.write("<tr><td><b>Start Time: </b></td><td>"+SuiteData.reportMap.get(testCaseName).getStartTime()+"</td>" );
		out.write("<td><b>Execution Time: </b></td><td>"+SuiteData.reportMap.get(testCaseName).getExecutionTime()+"</td></tr>");
		
		out.write("<tr><td><b>End Time:</b></td><td>"+SuiteData.reportMap.get(testCaseName).getEndTime()+"</td>");
		out.write("<td><b>Status: </b></td><td>"+SuiteData.reportMap.get(testCaseName).getStatus()+"</td></tr>");
		
		
		
		
		
		
				
		
		out.write("<table border = \"1\" style=\"width:100%\">");
		out.write("<tr>");
		out.write("<th>Step No</th>");
		out.write("<th>Keyword</th>"); 
		out.write("<th>Expected Result</th>");
		out.write("<th>Actual Result</th>");
		out.write("<th>Status</th>");
		out.write("</tr>");
		for(String key:hashmap.keySet())
		{
			out.write("<tr>");
			out.write("<td>"+key+"</td>");
			out.write("<td>"+hashmap.get(key).getAction()+"</td>");
			out.write("<td>"+hashmap.get(key).getExpectedResult()+"</td>");
			
			if(hashmap.get(key).getStatus().equalsIgnoreCase("pass"))
			{
				out.write("<td>"+hashmap.get(key).getActualResult()+"</td>");
				out.write("<td bgcolor=\"#44aa44\">Passed</td>");
			}
			else
			{
				out.write("<td>"+hashmap.get(key).getActualResult()+"<a href=../"+hashmap.get(key).getScreenShot()+"> :ScreenShot</a></td>");
				out.write("<td bgcolor=\"#FF0000\">Failed</td>");
			}
			out.write("</tr>");
		}
        out.write("</table>");
			
		out.write("<style>.google-visualization-table-td {text-align: center !important;}</style>");
		out.write("<div id=\"table_div\"></div>");
		out.write("</body>");
		out.write("</html>");
		out.close();
	}

}
