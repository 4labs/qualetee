package com.fourlabs.report;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.fourlabs.SuiteData;
import com.fourlabs.utils.FilePath;
public class ExtentReport implements FilePath{
	
	private static ExtentHtmlReporter extentReport;
	private static ExtentReports extent;
	private static ExtentTest logger;
	
	public static ExtentReports initExtentReport()
	{
		extentReport=new ExtentHtmlReporter(extentReportPath);
		extent=new ExtentReports();
		extent.attachReporter(extentReport);
		extentReport.config().setReportName("Qualetee Execution Report");
		extentReport.config().setTheme(Theme.STANDARD);
		extentReport.config().setTestViewChartLocation(ChartLocation.TOP);
		return extent;
	}
	
	public static void createTest(String testName)
	{
		logger=extent.createTest(testName);
	}
	public static void addPassedAssertion(String description)
	{
		logger.log(Status.PASS, description);
	}
	public static void addFailedAssertion(String description) throws IOException
	{
		TakesScreenshot scrShot =((TakesScreenshot)SuiteData.driver);
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		String timestamp = new SimpleDateFormat("yyyy_MM_dd__hh_mm_ss").format(new Date());
		String path=FilePath.screenShot+timestamp+".png";
		File DestFile=new File(FilePath.executionReport+path);
		FileUtils.copyFile(SrcFile, DestFile);
		SuiteData.step.setScreenShot(path);
		logger.addScreenCaptureFromPath(path);
		logger.log(Status.FAIL, description);
		
	}
	public static void addTestInformation(String description)
	{
		logger.log(Status.INFO, description);
	}
	public static void tearDown()
	{
		extent.flush();
	}
}
