package com.fourlabs.dto;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ModuleLevel {
	
	private String testCaseName;
	private String startTime;
	private String endTime;
	private String executionTime;
	private String status;
    private HashMap<String,LinkedHashMap<String,StepLevel>> stepMap;
	
	
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public HashMap<String,LinkedHashMap<String, StepLevel>> getStepMap() {
		return stepMap;
	}
	public void setStepMap(HashMap<String,LinkedHashMap<String, StepLevel>> stepMap) {
		this.stepMap = stepMap;
	}
	
}
