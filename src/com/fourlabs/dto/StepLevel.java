package com.fourlabs.dto;

import java.io.IOException;

import com.fourlabs.report.ExtentReport;

public class StepLevel {
	
	private String stepId;
	private String action;
	private String expectedResult;
	private String actualResult;
	private String status;
	private String screenShot;

	
	public String getStepId() {
		return stepId;
	}
	public String getScreenShot() {
		return screenShot;
	}
	public void setScreenShot(String screenShot) {
		this.screenShot = screenShot;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getExpectedResult() {
		return expectedResult;
	}
	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}
	public String getActualResult() {
		return actualResult;
	}
	public void setActualResult(String actualResult) {
		this.actualResult = actualResult;
		
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) throws IOException {
		this.status = status;
		if(status=="Fail")
		{
			ExtentReport.addFailedAssertion(actualResult);
		}
		else if(status=="Pass")
		{
			ExtentReport.addPassedAssertion(actualResult);
		}
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
}
