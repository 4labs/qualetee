package com.fourlabs.test;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.fourlabs.SuiteData;
import com.fourlabs.dto.ModuleLevel;
import com.fourlabs.report.ExtentReport;
import com.fourlabs.utils.FilePath;
import com.fourlabs.utils.ObjectUtils;

public class BasicSetup implements FilePath {
	
	

	@BeforeSuite
	public void initiateReportMap() throws Throwable
	{
		SuiteData.reportMap=new HashMap<String,ModuleLevel>();
		SuiteData.extent=ExtentReport.initExtentReport();
		//new ObjectUtils().setObjMap();
		
		
	}
	
	
	
	@BeforeMethod
	public void startDriver()
	{
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		SuiteData.driver=new ChromeDriver();
		SuiteData.driver.manage().window().maximize();
		
	}
	
	@AfterMethod
	public void closeDriver()
	{
		SuiteData.driver.quit();
	}
	
	@AfterSuite
	public void tearDown()
	{
	
	}
}
