package com.fourlabs.test;

import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fourlabs.executor.ScriptExecutor;


@Listeners(com.fourlabs.listeners.Listener.class)
public class TestClass extends BasicSetup{
	
	@Parameters({"TCName"})
	@Test(priority=1)
	public void Script(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}

	@Parameters({"Script1"})
	@Test(priority=1)
	public void Script1(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	
	@Parameters({"Script2"})
	@Test(priority=2)
	public void Script2(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	
	@Parameters({"Script3"})
	@Test(priority=3)
	public void Script3(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	
	@Parameters({"Script4"})
	@Test(priority=4)
	public void Script4(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	
	@Parameters({"Script5"})
	@Test(priority=5)
	public void Script5(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	@Parameters({"Script6"})
	@Test(priority=5)
	public void Script6(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	
	@Parameters({"Script7"})
	@Test(priority=6)
	public void Script7(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
	@Parameters({"Script8"})
	@Test(priority=7)
	public void Script8(String testcaseName) throws Throwable
	{
		new ScriptExecutor().readAndExecuteScript(testcaseName);
	}
}
