package com.fourlabs.utils;

import com.fourlabs.SuiteData;

public class ValueConverter {
	
	public String formatValue(String value)
	{
		if(value.startsWith("&"))
		{
			value=value.substring(1, value.length());
			return (String) SuiteData.json.get(value);
		}
		
		
		return value;
		
	}

}