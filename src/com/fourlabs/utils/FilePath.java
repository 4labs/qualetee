package com.fourlabs.utils;

public interface FilePath {
	
	public String userDir=System.getProperty("user.dir");
	public String testCaseFile=userDir+"\\testcase\\Demo.xlsx";
	public String chromeDriver=userDir+"\\libs\\chromedriver\\chromedriver.exe";
	public String executionReport=userDir+"\\report\\";
	public String testCaseReport=userDir+"\\report\\testcaseReport\\";
	public String objectRepo=userDir+"\\objectRepo\\ObjectRepo.json";
	public String testData=userDir+"\\testData\\";
	public String screenShot="screenShots\\";
	public String extentReportPath=userDir+"\\report\\extentReport\\ExtendReport.html";
	
}
