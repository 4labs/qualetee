package com.fourlabs.utils;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Fillo.Recordset;

import com.google.gson.JsonSyntaxException;
import com.fourlabs.SuiteData;
import com.fourlabs.dto.ObjectRepo;

public class ObjectUtils {

	

	JsonReader jRead=new JsonReader();
	
	
	public void setObjMap() throws Throwable
	{
		SuiteData.objMap=new HashMap<String,ObjectRepo>();
		ExcelUtils utils=new ExcelUtils();
		Recordset record=utils.filterWorkSheet("Objects");
		while (record.next())
		{
			ObjectRepo obj=new ObjectRepo();
			Recordset objRecord=utils.filterObjectSheet(record.getField("FileName"),record.getField("Sheet"), "ObjectID", record.getField("ObjectID"));
			objRecord.next();
			obj.setLocatorName(objRecord.getField("LocatorName"));
			obj.setLocatorValue(objRecord.getField("LocatorValue"));
			obj.setFieldName(objRecord.getField("FieldName"));
			SuiteData.objMap.put(record.getField("ObjectName").toString(), obj);
		}
		record.close();
	}
	
	public WebElement getElement(String key) throws JsonSyntaxException, IOException, ParseException
	{
		return getElement(key,"");
	}
	public WebElement getElement(String key,String replace) throws JsonSyntaxException, IOException, ParseException
	{
		String locator=jRead.readObjRepo(key, "LocatorName");
		String value=jRead.readObjRepo(key, "LocatorValue");
		if(!replace.isEmpty())
		value=value.replace("REPLACE", replace);
		WebElement element=null;
		WebDriverWait wait=new WebDriverWait(SuiteData.driver, 10);
		
		switch(locator.toLowerCase())
		{
		case "id":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(value)));
			break;
		case "name":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(value)));
			break;
		case "class":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(value)));
			break;
		case "tagname":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(value)));
			break;
		case "linktext":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(value)));
			break;
		case "partiallinktext":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(value)));
			break;
		case "css":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(value)));
			break;
		case "xpath":
			element=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value)));
			break;
		}
		return element;
		
	}
	

}
