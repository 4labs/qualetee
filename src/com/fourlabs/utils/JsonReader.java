package com.fourlabs.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;







import org.json.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import com.google.gson.JsonSyntaxException;

public class JsonReader implements FilePath
{
	public static void main(String[] args) throws JsonSyntaxException, IOException, ParseException {
		System.out.println(new JsonReader().readObjRepo("6","LocatorValue"));
	}
	
	public String readObjRepo(String objID,String attribute) throws JsonSyntaxException, IOException, ParseException
	{
		JSONParser jsonParser =  new JSONParser();
		Object obj = jsonParser.parse(new FileReader(objectRepo));
		JSONObject jsonObj=(JSONObject)obj;
		JSONArray array=(JSONArray) jsonObj.get("ObjectRepo");
		for(Object ob:array)
		{
			 JSONObject jsonOb=(JSONObject)ob; 
			 if(jsonOb.get("FieldName").toString().equalsIgnoreCase(objID))
			 {
				 return jsonOb.get(attribute).toString();
			 }
		}
		return null;
	}
	public JSONArray readTestData(String fileName) throws JsonSyntaxException, IOException, ParseException
	{
		JSONParser jsonParser =  new JSONParser();
		Object obj = jsonParser.parse(new FileReader(testData+fileName+".json"));
		JSONObject jsonObj=(JSONObject)obj;
		JSONArray array=(JSONArray) jsonObj.get("TestData");
		return array;
	}
}



//"E:/Eclipse_seislenium/HybidFramework/srrc/test/java/com/aditya/dataSource/Registration.json"